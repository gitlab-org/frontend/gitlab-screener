const Promise = require('bluebird');
const looksSame = require('looks-same');
const png = require('looks-same/lib/png');
const Color = require('color');

async function compareScreenshots(captures, baseCaptureDir) {
  await Promise.map(
    captures,
    (capture, index) => {
      return new Promise((resolve, reject) => {
        console.log();

        const goldenScreenshotPath = `${baseCaptureDir}/golden/${capture.urlToPath}.png`;
        const testScreenshotPath = `${baseCaptureDir}/testrun/${capture.urlToPath}.png`;
        const diffPath = `${baseCaptureDir}/diff/${capture.urlToPath}.png`;

        const diffOptions = {
          stopOnFirstFail: false,
          reference: goldenScreenshotPath,
          current: testScreenshotPath,
          highlightColor: '#ff0000', // color to highlight the differences
          strict: false, // strict comparsion
          tolerance: 2.5,
          antialiasingTolerance: 0,
          ignoreAntialiasing: true, // ignore antialising by default
          ignoreCaret: true, // ignore caret by default
        };

        looksSame(goldenScreenshotPath, testScreenshotPath, diffOptions, (error, result) => {
          if (error) {
            console.error('Error on comparison : ', error);
            reject(error);
          }
          const { equal, diffBounds, diffClusters } = result;
          if (!equal) {
            capture.isDifferent = true;
            capture.differences = diffClusters;

            looksSame.createDiff(diffOptions, (error, buffer) => {
              if (error) {
                console.error('Error on diff creation ', error);
                reject(error);
              }

              png.fromBuffer(buffer, {}, (err, result) => {
                const highColor = {
                  R: 255,
                  G: 0,
                  B: 0,
                };

                iterateRect(
                  result.width,
                  result.height,
                  (x, y) => {
                    const colorRes = result.getPixel(x, y);

                    // if (y === 0) console.log('C : ', colorRes);

                    if (!looksSame.colors(colorRes, highColor)) {
                      let whiteFactor = 0.8;

                      if (diffClusters) {
                        diffClusters.forEach(area => {
                          if (
                            x >= area.left &&
                            x <= area.right &&
                            y >= area.top &&
                            y <= area.bottom
                          ) {
                            whiteFactor = 0;
                          }
                        });
                      }

                      let newColor = Color({
                        r: colorRes.R,
                        g: colorRes.G,
                        b: colorRes.B,
                      }).mix(Color('white'), whiteFactor);

                      const newCExp = {
                        R: Math.round(newColor.red()),
                        G: Math.round(newColor.green()),
                        B: Math.round(newColor.blue()),
                      };

                      result.setPixel(x, y, newCExp);
                    }
                  },
                  () => {
                    result.save(diffPath, savedRes => {
                      console.log(
                        `${index + 1}/${captures.length} - Screenshot mismatch on ${
                          capture.fullUrl
                        } (${capture.device})`
                      );
                      resolve();
                    });
                  }
                );
              });
            });
          } else {
            console.log(
              `${index + 1}/${captures.length} - Compared ${capture.fullUrl} (${
                capture.device
              }) successfully`
            );
            resolve();
          }
        });
      });
    },
    { concurrency: 2 }
  );
}

const iterateRect = (width, height, callback, endCallback) => {
  const processRow = y => {
    setImmediate(() => {
      for (let x = 0; x < width; x++) {
        callback(x, y);
      }

      y++;

      if (y < height) {
        processRow(y);
      } else {
        endCallback();
      }
    });
  };

  processRow(0);
};

module.exports = compareScreenshots;
