const captureWebsite = require('capture-website');
const Promise = require('bluebird');

const defaultCaptureOptions = {
  width: 1920,
  height: 1000,
  fullPage: true,
  delay: 3,
  overwrite: true,
  hideElements: ['.fa-spinner', '#js-peek', '.js-timeago', '.updated-note'],
  removeElements: ['#js-peek'],
  modules: [
    `
    document.documentElement.classList.remove('with-performance-bar')
    `,
  ],
  launchOptions: {
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
};

async function takeScreenshots(captures, config, saveDir) {
  console.log('\nStarting taking ' + captures.length + ' Screenshots\n');
  await Promise.map(
    captures,
    (capture, index) => {
      console.log(`${index + 1}/${captures.length} Capturing ${capture.url} on ${capture.device}`);

      const captureUrl = config.baseUrl + capture.url;
      const screenshotPath = `${saveDir}/${capture.urlToPath}.png`;

      const captureOptions = { ...defaultCaptureOptions };
      if (capture.device != 'desktop') {
        captureOptions.emulateDevice = capture.device;
      }
      return captureWebsite.file(captureUrl, screenshotPath, captureOptions);
    },
    { concurrency: 2 }
  );

  console.log('\nFinished ' + captures.length + ' Screenshots');
}

module.exports = takeScreenshots;
