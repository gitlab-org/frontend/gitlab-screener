const fs = require('fs');

const git = require('simple-git/promise');
const open = require('open');
const httpServer = require('http-server');
const Promise = require('bluebird');

const config = require('./run-config');

const takeScreenshots = require('./src/takeScreenshots');
const compareScreenshots = require('./src/compareScreenshots');
const createReport = require('./src/createReport');

let isGoldenMaster = false;
let onlyComparison = false;

// Check command line arguments
process.argv.forEach(val => {
  if (val === '--gold' || val === '-G') {
    isGoldenMaster = true;
  } else if (val === '--compare' || val === '-C') {
    onlyComparison = true;
  }
});

// Create capture array
let captures = [];
config.urls.map(url => {
  if (config.devices) {
    let deviceCaptures = config.devices.map(device => {
      return {
        device,
        url,
      };
    });
    captures = captures.concat(deviceCaptures);
  }
  captures.push({ device: 'desktop', url });
});

captures.forEach(capture => {
  capture.fullUrl = config.baseUrl + capture.url;

  capture.isDifferent = false;
  capture.differences = null;

  capture.urlToPath = capture.url.replace('/', '').replace(/\//gi, '-');
  if (capture.device) {
    capture.urlToPath += `-${capture.device.replace(/ /gi, '')}`;
  }
});

(async () => {
  // Getting Branch Name
  let gitBranchName = 'master';
  try {
    const gitlabGit = git(config.pathToRepo);
    const statusSummary = await gitlabGit.status();
    if (statusSummary && statusSummary.current) {
      gitBranchName = statusSummary.current;
      console.log(`Detected Branch "${gitBranchName}" in "${config.pathToRepo}"`);
    }
  } catch (e) {
    console.error('Error with Git Status : ', e);
  }

  // Initialising Values
  let baseCaptureDir = `./captures/${gitBranchName}`;
  let actualCaptureDir = isGoldenMaster ? '/golden' : '/testrun';
  fs.mkdirSync(baseCaptureDir + actualCaptureDir, { recursive: true });
  fs.mkdirSync(baseCaptureDir + '/diff', { recursive: true });

  //  Taking Screenshots
  if (!onlyComparison) {
    await takeScreenshots(captures, config, baseCaptureDir + actualCaptureDir);
  }

  if (!isGoldenMaster) {
    console.log('\nStarting Comparison ...');

    // Comparing the screenshots
    await compareScreenshots(captures, baseCaptureDir);

    // Creating the report
    createReport(captures, 'Screenshot diff report for branch ' + gitBranchName, baseCaptureDir);

    if (config.startWebserver) {
      var server = httpServer.createServer({
        root: baseCaptureDir,
        autoIndex: false,
        showDir: false,
        ext: 'htm',
        cache: -1,
      });
      server.listen(8080);

      if (config.startBrowser) {
        await open('http://localhost:8080');
      }

      console.log('\nServing web server with report on http://localhost:8080');
    }
  }

  console.log('Done');
})();
